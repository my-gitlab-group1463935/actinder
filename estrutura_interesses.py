database = {} #um dicionário, que tem a chave interesses para o controle
#dos interesses (que pessoa se interessa por que outra), e pessoas para o controle de pessoas (quem sao as pessoas e quais sao os dados pessoais de cada pessoa no sistema)
#voce pode controlar as pessoas de outra forma se quiser, nao precisa mudar nada
#do seu código para usar essa váriavel
database['interesses'] = { 
    100: [101, 102, 103],
    200: [100]
}
database['PESSOA'] = [] #esse voce só faz se quiser guardar nessa 
#lista os dicionários das pessoas

#em todo esse codigo que estou compartilhando, as variaveis interessado, alvo de interesse, pessoa, pessoa1 e pessoa2 sao sempre IDs de pessoas
class IncompatibleError(Exception):
    pass
class NotFoundError(Exception):
    pass

def todas_as_pessoas():
    return database['PESSOA']

def adiciona_pessoa(dic_pessoa):
    database['PESSOA'].append(dic_pessoa)
    id_pessoa = dic_pessoa['id']
    database['interesses'][id_pessoa] = []

def localiza_pessoa(id_pessoa):
    for dic_pessoa in database['PESSOA']:
        if dic_pessoa['id'] == id_pessoa:
            return dic_pessoa
    raise NotFoundError

def reseta():
    database['PESSOA'] = []

def adiciona_interesse(id_interessado, id_alvo_de_interesse):
    d_interessado = localiza_pessoa(id_interessado) #nao tem um try. Se a localiza_pessoa
    #der raise, a adiciona_interesse dá tambem
    d_alvo = localiza_pessoa(id_alvo_de_interesse)
    if 'sexo' in d_alvo.keys() and 'buscando' in d_interessado.keys():
        if not d_alvo['sexo'] in d_interessado['buscando']:
            raise IncompatibleError
    database['interesses'][id_interessado].append(id_alvo_de_interesse)     
                     


def consulta_interesses(id_interessado):
    localiza_pessoa(id_interessado)
    return database['interesses'][id_interessado]
    

def remove_interesse(id_interessado,id_alvo_de_interesse):
    localiza_pessoa(id_interessado)
    localiza_pessoa(id_alvo_de_interesse)
    database['interesses'][id_interessado].remove(id_alvo_de_interesse)




#essa funcao diz se o 1 e o 2 tem match. (retorna True se eles tem, False se não)
#ela não está testada, só existe para fazer aquecimento para a próxima
def verifica_match(id1,id2):
    lista1 = consulta_interesses(id1)
    lista2 = consulta_interesses(id2)
    if (id1 in lista2 and id2 in lista1): return True
    return False

      
def lista_matches(id_pessoa1):   #id_pessoa = 13
    likes_pessoa1 = consulta_interesses(id_pessoa1) # [33,35]
    matches = []
    for outra_pessoa in likes_pessoa1:  #33
        if verifica_match(id_pessoa1,outra_pessoa): #verifica_match(13,33)
            matches.append(outra_pessoa)
    return matches




