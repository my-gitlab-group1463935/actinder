from flask import Flask, jsonify, request
import estrutura_interesses as i

app = Flask(__name__)
app.url_map.strict_slashes = False

"http://localhost:5003/pessoas"


@app.route("/")
def ola():
    return "servidor do tinder"


@app.route("/pessoas", methods=['GET'])
def todas_pessoas():
    return i.todas_as_pessoas()


@app.route("/pessoas", methods=['POST'])
def adiciona_pessoa():
    dic_enviado = request.json
    i.adiciona_pessoa(dic_enviado)
    return "ok"


@app.route("/pessoas/<int:id_pessoa>", methods=['GET'])
def pessoa_por_id(id_pessoa):
    try:
        pessoa = i.localiza_pessoa(id_pessoa)
        return pessoa 
    except:
        return 'pessoa não existe 404' , 404


@app.route("/reseta", methods=['POST'])
def resetar():
    i.reseta()
    return 'ok' 
    



"http://localhost:5003/sinalizar_interesse"



@app.route("/sinalizar_interesse/<int:id_pessoa1>/<int:id_pessoa2>", methods=['POST'])
def adiciona_interesse(id_pessoa1, id_pessoa2):
    interessado = int(id_pessoa1)
    alvo = int(id_pessoa2)
    try:
        i.adiciona_interesse(interessado,alvo)
        return 'ok'
    except:
        return 'ERRO' , 404



@app.route("/sinalizar_interesse/<int:id_pessoa1>", methods=['GET'])
def interesses(id_pessoa1):
    interessado = id_pessoa1
    try:
        lista = i.consulta_interesses(interessado)
        return lista
    except:
        return 'ERRO', 404



@app.route("/sinalizar_interesse/<int:id_pessoa1>/<int:id_pessoa2>", methods=['DELETE'])
def deleta_interessess(id_pessoa1, id_pessoa2):
    interessado = int(id_pessoa1)
    alvo = int(id_pessoa2)
    try:
        i.remove_interesse(interessado,alvo)
        return 'ok'
    except:
        return 'ERRO'


"http://localhost:5003/matches"

@app.route("/matches/<int:id_pessoa1>", methods=['GET'])
def lista_matches(id_pessoa1):
    interessado = int(id_pessoa1)
    try:
        return i.lista_matches(interessado)
    except:
        return 'ERRO' , 404





    

    


if __name__ == '__main__':
    app.run(host='localhost', port=5003, debug=True)


